$('.big-slid').slick({
    arrows: true,
    dots: true,
    autoplay:true,
    speed: 1000,
    autoplaySpeed: 2500
});
$('.slid-gallery').slick({
    arrows: true,
    dots: true,
    autoplay:true,
    speed: 1000,
    autoplaySpeed: 2500,
    variableWidth: true,
    focusOnSelect: true
});
$('.item-slid-gallery').slick({
    arrows: true,
    dots: true,
    autoplay:false,
    speed: 1000,
    autoplaySpeed: 2000,
    focusOnSelect: true
});

$('#lightgallery').on('click', function() {

    $(this).lightGallery({
        thumbnail:true,
        dynamic: true,
        dynamicEl: [{
            "src": '../img/news/img1.png',
            'thumb': '../img/news/img1.png'
        }, {
            'src': '../img/news/img2.png',
            'thumb': '../img/news/img2.png'
        },  {
            'src': '../img/news/img3.png',
            'thumb': '../img/news/img3.png'
        }]
    })

});
$(window).on('load',function(){
    $('.str9').liMarquee({
        direction: 'left',
        circular:true,
        startShow:true,
        //scrolldelay: 10,
        scrollamount: 100
    });
})
$('#lightgallery2').on('click', function() {

    $(this).lightGallery({
        thumbnail:true,
        dynamic: true,
        dynamicEl: [{
            "src": '../img/news/img2.png',
            'thumb': '../img/news/img2.png'
        }, {
            'src': '../img/news/img1.png',
            'thumb': '../img/news/img1.png'
        },  {
            'src': '../img/news/img3.png',
            'thumb': '../img/news/img3.png'
        }]
    })

});
$('#lightgallery3').on('click', function() {

    $(this).lightGallery({
        thumbnail:true,
        dynamic: true,
        dynamicEl: [{
            "src": '../img/news/img3.png',
            'thumb': '../img/news/img3.png'
        }, {
            'src': '../img/news/img2.png',
            'thumb': '../img/news/img2.png'
        },  {
            'src': '../img/news/img1.png',
            'thumb': '../img/news/img1.png'
        }]
    })

});
$('.ic-menu').click(function () {
    $('.mob-menu').slideToggle(100)
});
$('.ic-search').click(function () {
    $('.mob-search').slideToggle(100)
});

// $(window).load(function(){
// $('.str').liMarquee();
// });
jQuery(document).ready(function($){
    $.bvi({
        'bvi_target': '.bvi-open',
        "bvi_theme":"white",
        "bvi_font":"arial",
        "bvi_font_size":16,
        "bvi_letter_spacing":"normal",
        "bvi_line_height":"normal",
        "bvi_images":true,
        "bvi_reload":false,
        "bvi_fixed":false,
        "bvi_voice":true,
        "bvi_flash_iframe":true,
        "bvi_hide":false
    });
});
var ctx = document.createElement('canvas').getContext('2d');
var linGrad = ctx.createLinearGradient(0, 64, 0, 200);
linGrad.addColorStop(0.5, 'rgba(255, 255, 255, 1.000)');
linGrad.addColorStop(0.5, 'rgba(183, 183, 183, 1.000)');

var wavesurfer = WaveSurfer.create({
    // Use the id or class-name of the element you created, as a selector
    container: '#waveform',
    // The color can be either a simple CSS color or a Canvas gradient
    waveColor: linGrad,
    progressColor: 'hsla(200, 100%, 30%, 0.5)',
    cursorColor: '#fff',
    // This parameter makes the waveform look like SoundCloud's player
    barWidth: 3
});

wavesurfer.on('loading', function (percents) {
    document.getElementById('progress').value = percents;
});

wavesurfer.on('ready', function (percents) {
    document.getElementById('progress').style.display = 'none';
});

wavesurfer.load('https://wavesurfer-js.org/example/media/demo.wav');

